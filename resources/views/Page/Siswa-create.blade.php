<!DOCTYPE html>
<html lang="en">

<head>

@include('Layout.head')

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('Layout.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                    @include('Layout.navbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0">Siswa</h1>
                                </div><!-- /.col -->
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                                        <li class="breadcrumb-item active">Data Siswa</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content-header -->
        
                    <!-- Main content -->
                    <div class="content">
                        <div class="card card-info card-outline">
                            <div class="card-header">
                                <h3>Tambah Data Siswa</h3>
        
                            </div>
                            <div class="card-body">
                               <form action="{{route('Siswa-store')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" id="id" name="id" class="form-control" placeholder="id">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="nis" name="nis" class="form-control" placeholder="nis">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="nama" name="nama" class="form-control" placeholder="nama">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="gender" name="gender" class="form-control" placeholder="gender">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" placeholder="tempat_lahir">
                                    </div>
                                    <div class="form-group">
                                        <input type="date" id="tgl_lahir" name="tgl_lahir" class="form-control" placeholder="tgl_lahir">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="email" name="email" class="form-control" placeholder="email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="nama_ortu" name="nama_ortu" class="form-control" placeholder="nama_ortu">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="phone_number">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="kelas_id" name="kelas_id" class="form-control" placeholder="kelas_id">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success">Simpan Data</button>
                                    </div>
                                    
                               </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            @include('Layout.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    @include('Layout.script')

</body>

</html>