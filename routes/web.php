<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Dashbord.Beranda');
})->name('start');

Route::get('/login', function () {
    return view('Awal.Login');
})->name('login');

Route::post('/postlogin', 'LoginController@postlogin')->name('postlogin');
Route::get('/logout', 'LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    route::get('/Beranda', 'BerandaController@index');
});

Route::get('/Guru-data', 'GuruController@index')->name('Guru-data');
Route::get('/Guru-create', 'GuruController@create')->name('Guru-create');
Route::post('/Guru-store', 'GuruController@store')->name('Guru-store');
Route::get('/Guru/{id}/edit', 'GuruController@edit')->name('Guru-edit');
Route::patch('/Guru/{id}', 'GuruController@update')->name('Guru-update');
Route::get('/Guru/{id}', 'GuruController@destroy')->name('Guru-delete');

Route::get('/Mapel-data', 'MapelController@index')->name('Mapel-data');
Route::get('/Mapel-create', 'MapelController@create')->name('Mapel-create');
Route::post('/Mapel-store', 'MapelController@store')->name('Mapel-store');
Route::get('/Mapel/{id}/edit', 'MapelController@edit')->name('Mapel-edit');
Route::patch('/Mapel/{id}', 'MapelController@update')->name('Mapel-update');
Route::get('/Mapel/{id}', 'MapelController@destroy')->name('Mapel-delete');


Route::get('/Siswa-data', 'SiswaController@index')->name('Siswa-data');
Route::get('/Siswa-create', 'SiswaController@create')->name('Siswa-create');
Route::post('/Siswa-store', 'SiswaController@store')->name('Siswa-store');
Route::get('/Siswa/{id}/edit', 'SiswaController@edit')->name('Siswa-edit');
Route::patch('/Siswa/{id}', 'SiswaController@update')->name('Siswa-update');
Route::get('/Siswa/{id}', 'SiswaController@destroy')->name('Siswa-delet');

Route::get('/Kelas-data', 'KelasController@index')->name('Kelas-data');
Route::get('/Kelas-create', 'KelasController@create')->name('Kelas-create');
Route::post('/Kelas-store', 'KelasController@store')->name('Kelas-store');
Route::get('/Kelas/{id}/edit', 'KelasController@edit')->name('Kelas-edit');
Route::patch('/Kelas/{id}', 'KelasController@update')->name('Kelas-update');
Route::get('/Kelas/{id}', 'KelasController@destroy')->name('Kelas-delete');

Route::get('/Jadwal-data', 'JadwalController@index')->name('Jadwal-data');
Route::get('/Jadwal-create', 'JadwalController@create')->name('Jadwal-create');
Route::post('/Jadwal-store', 'JadwalController@store')->name('Jadwal-store');
Route::get('/Jadwal/{id}/edit', 'JadwalController@edit')->name('Jadwal-edit');
Route::patch('/Jadwal/{id}', 'JadwalController@update')->name('update-jadwal');
Route::get('/Jadwal/{id}', 'JadwalController@destroy')->name('delete-jadwal');

