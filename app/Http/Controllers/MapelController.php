<?php

namespace App\Http\Controllers;

use App\Mapel;
use Illuminate\Http\Request;

class MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Mapel = Mapel::all();
        return view('Page.Mapel-data', compact('Mapel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Page.Mapel-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Mapel::create([
            'id' => $request->id,
            'kode_mapel' => $request->kode_mapel,
            'nama_mapel' => $request->nama_mapel,

        ]);
        return redirect('Mapel-data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Mapel = Mapel::find($id);
        return view('Page.Mapel-edit', compact('Mapel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Mapel = Mapel::find($id);
        $Mapel->id = $request ->input('id');
        $Mapel->kode_mapel = $request ->input('kode_mapel');
        $Mapel->nama_mapel = $request ->input('nama_mapel');
        $Mapel->save();
        
        return redirect ('Mapel-data')->with('success', 'Data Berhasil Diubah');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Mapel = Mapel::findorfail($id);
        $Mapel->delete();
        return redirect ('Mapel-data')->with('success', 'Data Berhasil Dihapus');;
    }
}