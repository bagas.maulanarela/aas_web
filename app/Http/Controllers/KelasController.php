<?php

namespace App\Http\Controllers;

use App\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Kelas = Kelas::all();
        return view('Page.Kelas-data', compact('Kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Page.Kelas-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Kelas::create([
            'id' => $request->id,
            'kode_kelas' => $request->kode_kelas,
            'nama_kelas' => $request->nama_kelas,

        ]);
        return redirect('Kelas-data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Kelas = Kelas::find($id);
        return view('Page.Kelas-edit', compact('Kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Kelas = Kelas::find($id);
        $Kelas->id = $request ->input('id');
        $Kelas->kode_kelas = $request->input('kode_kelas');
        $Kelas->nama_kelas = $request->input('nama_kelas');
        $Kelas->save();
            
            return redirect ('Kelas-data')->with('success', 'Data Berhasil Diubah');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Kelas = Kelas::findorfail($id);
        $Kelas->delete();
        return redirect ('Kelas-data')->with('success', 'Data Berhasil Dihapus');;
    }
}